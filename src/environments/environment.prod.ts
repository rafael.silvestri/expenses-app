export const environment = {
  production: true,
  apiUrl: "http://localhost:8080/api/expenses/",
  viaCepUrl: "https://viacep.com.br/ws/"
};
