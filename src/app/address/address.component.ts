import { Component, OnInit, Input } from '@angular/core';
import { AddressService } from '../address.service';
import { Address } from '../address.model';

@Component({
  selector: 'address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})
export class AddressComponent implements OnInit {

  constructor(private addressService: AddressService) { }
  @Input() address: Address = new Address();

  ngOnInit() {
  }

  /**
   * Retrives the address from 'viacep' for a given cep 
   * @param cep cep to query over address
   */
  updateAddress(cep) {
    if (this.address.cep) {
      this.addressService.getAddressByCep(this.address.cep).subscribe(data => {
        this.address = data;
      }, err => {
        console.log(err);
      });
    }
  }

}
