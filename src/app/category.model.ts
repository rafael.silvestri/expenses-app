export class Category {
    id: number;
    name: string;
    active: boolean;

    constructor() { }

    isNew() {
        return true;
    }

}
