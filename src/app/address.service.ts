import { Injectable } from '@angular/core';
//import { Http, Response } from '@angular/http';
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Observable } from 'rxjs/Observable'

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import { Address } from './address.model';
import { environment } from '../environments/environment';

@Injectable()
export class AddressService {

  //constructor(private http: Http) { }
  constructor(private http: HttpClient) { }

  /**
  * Retrives an address given a cep
  * @param cep cep to query over address
  */
  getAddressByCep(cep): Observable<Address>{
    return this.http
      .get<Address>(environment.viaCepUrl + cep + "/json")
      //.do(data =>console.log('All : ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  private handleError(error: HttpErrorResponse)
  {
    //debugger;
    console.error(error);
    let errorMessage = '';
    if (error.error instanceof Error) {
        // A client-side or network error occurred. Handle it accordingly.
        errorMessage = 'An error occurred: ${error.error.message}';
    } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        errorMessage = 'Server returned code: ${error.status}, error message is: ${error.message}';
    }
    console.error(errorMessage);
    return Observable.throw(errorMessage);
}

}
