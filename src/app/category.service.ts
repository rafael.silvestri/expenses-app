import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http'
import { Observable } from 'rxjs/Observable'

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';

import { Category } from './category.model';
import { environment } from '../environments/environment';

@Injectable()
export class CategoryService {
  headers: HttpHeaders;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Accept': 'application/json' });
  }

  /**
  * Retrives a list of categories
  */
  getCategories(): Observable<Category[]> {
    //"assets/mock/categories.json"
    return this.http
      .get<Category[]>(environment.apiUrl + "v1/categories");
  }

  create(category: Category): Observable<any> {
    return this.http.post(environment.apiUrl + "v1/categories", category, { headers: this.headers });
  }

  update(category: Category): Observable<any> {
    //let httpParams = new HttpParams()
    //.set('id', category.id.toString());
    return this.http.put(environment.apiUrl + "v1/categories/" + category.id, category,
      { headers: this.headers });
  }

  delete(category: Category): Observable<any> {
    //let httpParams = new HttpParams()
    //.set('id', category.id.toString());
    return this.http.delete(environment.apiUrl + "v1/categories/" + category.id,
      { headers: this.headers });
  }

}
