/**
 * Represents an address that comes from https://viacep.com.br
 */
export class Address {
    cep: string;
    logradouro: string;
    bairro: string;
    localidade: string;
    uf: string;
    complemento: string;
    ibge: string;
    unidade: string;

}
