import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from "./home/home.component";
import { CategoryComponent } from './category/category.component';
import { LoginComponent } from './login/login.component';
import { SupportComponent } from './support/support.component';
import { IncomeComponent } from './income/income.component';
import { ExpensesComponent } from './expenses/expenses.component';
import { VendorComponent } from './vendor/vendor.component';
import { AccountComponent } from './account/account.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'income',
    component: IncomeComponent
  },
  {
    path: 'expenses',
    component: ExpensesComponent
  },
  {
    path: 'categories',
    component: CategoryComponent
  },
  {
    path: 'vendors',
    component: VendorComponent
  },
  {
    path: 'accounts',
    component: AccountComponent
  },
  {
    path: 'support',
    component: SupportComponent
  },
  {
    path: 'login',
    component: LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
