import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../category.service';
import { Category } from '../category.model';
import { Message } from 'primeng/components/common/api';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  displayDialog: boolean;

  category: Category = new Category();
  categories: Category[];
  msgs: Message[] = [];

  constructor(private categoryService: CategoryService) { }

  ngOnInit() {
    this.refreshCategories();
  }

  refreshCategories() {
    this.categoryService.getCategories().subscribe(data => {
      this.categories = data;
    }, err => {
      console.log(err);
    })
  }

  add() {
    this.category = new Category();
    this.displayDialog = true;
  }

  edit(cat: Category) {
    this.category = cat;
    this.displayDialog = true;
  }

  save() {
    if (this.category.id) {
      this.categoryService.update(this.category).subscribe(data => {
        this.onSuccess(data);
      }, err => {
        this.onError(err);
      })
    } else {
      this.categoryService.create(this.category).subscribe(data => {
        this.onSuccess(data);
      }, err => {
        this.onError(err);
      })
    }
  }

  onSuccess(data) {
    this.category = data;
    this.category = new Category();
    this.displayDialog = false;
    this.refreshCategories();
  }

  onError(err) {
    console.log(err);
    this.msgs = [];
    this.msgs.push({ severity: 'error', summary: 'Erro inesperado', detail: err.status });
  }

  delete() {
    this.categoryService.delete(this.category).subscribe(data => {
      this.onSuccess(data);
    }, err => {
      this.onError(err);
    });
  }

  cancel() {
    this.displayDialog = false;
    this.category = new Category();
  }
}
