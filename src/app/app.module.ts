import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
//import { AngularFontAwesomeModule } from 'angular-font-awesome';

// PrimeNG Components
import {ButtonModule} from 'primeng/primeng';
import {DataTableModule,SharedModule,DialogModule} from 'primeng/primeng';
import {MessagesModule} from 'primeng/primeng';
import {MessageModule} from 'primeng/primeng';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AddressComponent } from './address/address.component';

import { AddressService } from './address.service';
import { CategoryComponent } from './category/category.component';
import { LoginComponent } from './login/login.component';
import { FooterComponent } from './footer/footer.component';
import { SideNavbarComponent } from './side-navbar/side-navbar.component';
import { HeaderComponent } from './header/header.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { VendorComponent } from './vendor/vendor.component';
import { ExpensesComponent } from './expenses/expenses.component';
import { IncomeComponent } from './income/income.component';
import { AccountComponent } from './account/account.component';
import { SupportComponent } from './support/support.component';
import { CategoryService } from './category.service';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    AddressComponent,
    CategoryComponent,
    LoginComponent,
    FooterComponent,
    SideNavbarComponent,
    HeaderComponent,
    DashboardComponent,
    VendorComponent,
    ExpensesComponent,
    IncomeComponent,
    AccountComponent,
    SupportComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    ButtonModule,
    DataTableModule,
    SharedModule,
    DialogModule,
    MessagesModule,
    MessageModule
    //AngularFontAwesomeModule
  ],
  providers: [AddressService, CategoryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
